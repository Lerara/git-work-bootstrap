module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
           dist: {
                src: [
                    'js/libs/*.js', // Все JS в папке libs
                    'js/global.js'  // Конкретный файл
                ],
                dest: 'js/build/production.js',
            }
        },
        uglify: {
            build: {
                src: 'js/build/production.js',
                dest: 'js/build/production.min.js'
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'images/build/'
                }]
            }
        },
        less: {
            development: {
                options: {
                    paths:["style"]
                },
                files: {
                  "style/main.css": "style/main.less" // destination file and source file
                }
            }
        },
        watch: {
            scripts: {
                files: ['style/*.less'],
                tasks: ['less','uglify','concat',],

                options: {
                    spawn: false,
                },
            }
        },

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');



    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале

    grunt.registerTask('default', ['concat', 'uglify', 'imagemin', 'watch', 'less']);

};